# Soyons fou ! Et si Drupal faisait moins de chose au runtime ?

Tout cela est un exercice de pensée.

Et si nous pouvions avoir comme une phase de compilation en amont plutôt que pendant l’exécution d’une requête HTTP ?
Et sans nécessité d’une application installée. Quels seraient les impacts ?

On va commencer par rêver en voyant ensemble ce qui pourrait simplifier notre vie en tant que développeur,
simplifier le développement d’outils comme phpstan-drupal.

Puis on commencera à réfléchir à ce qu’il faudrait faire pour atteindre ce rêve.
Que faudrait-il faire pour mettre à disposions de la communauté cela ?
Quel serait ce chantier ?

Ça ne sera pas vraiment de l’apprentissage de quelque chose en particulier.
Plutôt un voyage à la poursuite d’un rêve pour améliorer son quotidien.

Après certainement qu’il y aura des choses apprises par rapport au fonctionnement interne de Drupal,
ou encore sur la difficulté de certaines fonctionnalités dans phpstan-drupal.

.PHONY: start
start:
	nix-shell

.PHONY: format-and-pbcoby
format-and-pbcopy:
	pygmentize -O style=github-light-high-contrast -f rtf "${file}" | pbcopy

<?php

$link = $this->createMock(LinkItemInterface::class);
$link->method('isEmpty')
    ->willReturn(FALSE);
$link->method('__isset')
    ->with('uri')
    ->willReturn(TRUE);
$link->method('__get')
    ->with('uri')
    ->willReturn($faker->url());

$article = $this->createMock(Article::class);
$article->method('get')
    ->with('field_link')
    ->willReturn($link);
$article->method('isPublished')
    ->willReturn(TRUE);

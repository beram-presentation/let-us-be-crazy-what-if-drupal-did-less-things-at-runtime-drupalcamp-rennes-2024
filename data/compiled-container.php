<?php

namespace ContainerNV6rTbr;

class getFooService extends App_KernelProdContainer {
  public static function do($container) {
    return $container->privates['App\\Foo'] = new \App\Foo(
      $container->privates['bar'] ?? $container->load('getBarService')
    );
  }
}

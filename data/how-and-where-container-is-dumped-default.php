<?php

$settings['php_storage']['cache_container'] = [
    'class' => '\Drupal\Component\PhpStorage\FileStorage',
    'arguments' => [
        [
            'directory' => '/someplace/outside/docroot/php',
            'bin' => 'container',
        ],
    ],
];
$settings['bootstrap_container_definition'] = [
    'parameters' => [],
    'services' => [
        // [...]
        'cache.backend.php_factory' => [
            'class' => '\Drupal\Core\Cache\PhpBackendFactory',
            'arguments' => ['@cache_tags_provider.container'],
        ],
        'cache.container' => [
            'class' => '\Drupal\Core\Cache\PhpBackend',
            'factory' =>  ['@cache.backend.php_factory', 'get'],
            'arguments' => ['container'],
        ],
    ],
];

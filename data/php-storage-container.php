<?php

/** @var object{
 *   'cid': string,
 *   'data': array{
 *      'aliases': array<string, string>,
 *      'parameters': array<string, mixed>,
 *      'services': array<string, string>,
 *      'frozen': bool,
 *      'machine_format': bool,
 *   },
 *   'expire': int,
 *   'tags': array,
 *   'checksum': int
 * } $container
 */
$content = sprintf(
    '<?php return unserialize(%s);',
    var_export(serialize($container), TRUE)
);
